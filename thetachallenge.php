<?php
//Calendar Matching Challenge
//Input variables
$myCalendar = [['9:00', '10:30'],['12:00', '13:00'], ['16:00', '18:00']];
$myDailyBounds = [['9:00', '20:30']];
$coWorkerCalendar = [['10:00', '11:30'],['12:30', '14:30'], ['14:30', '15:00'], ['16:00', '17:00']];
$coWorkerDailyBounds = [['10:00', '18:30']];
//The meeting duration is a specific number of minutes. Ex. If the meeting is a 2 hour meeting set the meeting duration to 120
$meetingDuration = 30;

$myDailyStart = strtotime($myDailyBounds[0][0]);
$myDailyFinish = strtotime($myDailyBounds[0][1]);

$myMeetings = array();
$count = 0;
foreach ($myCalendar as $meeting) {
    $myMeetings[$count]['meetingStart'] = strtotime($meeting[0]);
    $myMeetings[$count]['meetingEnd'] = strtotime($meeting[1]);
    $count++;
}

$coWorkerDailyStart = strtotime($coWorkerDailyBounds[0][0]);
$coWorkerDailyFinish = strtotime($coWorkerDailyBounds[0][1]);

$coWorkerMeetings = array();
$count = 0;
foreach ($coWorkerCalendar as $meeting) {
    $coWorkerMeetings[$count]['meetingStart'] = strtotime($meeting[0]);
    $coWorkerMeetings[$count]['meetingEnd'] = strtotime($meeting[1]);
    $count++;
}
//These set the start time to be the later of my and my co-worker's start times, and the end time to be the earlier of the two end times
$startTime = $myDailyStart > $coWorkerDailyStart ? $myDailyStart : $coWorkerDailyStart;
$endTime = $myDailyFinish < $coWorkerDailyFinish ? $myDailyFinish : $coWorkerDailyFinish;
$currentTime = $startTime;

$optionalMeetingTimes = array();
$optionalMeetingStarts = array();
//This while loop checks every 15 minute interval agains my and my co-worker's meetings and logs the times that don't conflict with any meeting
while ($currentTime + ($meetingDuration*60) <= $endTime) {
    $notMeetingTime = true;
    foreach ($myMeetings as $meeting) {
        if($currentTime >= $meeting['meetingStart'] && $currentTime < $meeting['meetingEnd']) {
            $notMeetingTime = false;
        }
    }
    foreach ($coWorkerMeetings as $meeting) {
        if($currentTime >= $meeting['meetingStart'] && $currentTime < $meeting['meetingEnd']) {
            $notMeetingTime = false;
        }
    }

    if ($notMeetingTime) {
        $optionalMeetingStarts[] = $currentTime;
    }
    $currentTime = strtotime('+15 minutes', $currentTime);
}

//This foreach loop takes each of the times found in the above while loop and checks to see if there are any meetings that conflict with that time + the meeting duration
foreach ($optionalMeetingStarts as $optionalMeetingStart) {
    $optionalMeetingEnd = $optionalMeetingStart + ($meetingDuration*60);
    $noIntersectingMeeting = true;
    foreach ($myMeetings as $meeting) {
        if($meeting['meetingStart'] >= $optionalMeetingStart && $meeting['meetingStart'] < $optionalMeetingEnd) {
            $noIntersectingMeeting = false;
        }
    }
    foreach ($coWorkerMeetings as $meeting) {
        if($meeting['meetingStart'] >= $optionalMeetingStart && $meeting['meetingStart'] < $optionalMeetingEnd) {
            $noIntersectingMeeting = false;
        }
    }

    if($noIntersectingMeeting) {
        $optionalMeetingTime = array($optionalMeetingStart, $optionalMeetingEnd);
        $optionalMeetingTimes[] = $optionalMeetingTime;
    }
}

//This code takes the results of the above foreach loop and consolidates any times that overlap.
// Ex. If I found [['11:30, '12:00'],['15:00, '15:30'], ['15:15', 15:45], ['15:30', '16:00'], ['18:00, '18:30']]
// then this code consolidates that into [['11:30, '12:00'],['15:00, '16:00'], ['18:00, '18:30']]
usort($optionalMeetingTimes, function($a, $b)
{
    return $a[0] - $b[0];
});

$n = 0; $len = count($optionalMeetingTimes);
for ($i = 1; $i < $len; ++$i)
{
    if ($optionalMeetingTimes[$i][0] > $optionalMeetingTimes[$n][1] + (15*60))
        $n = $i;
    else
    {
        if ($optionalMeetingTimes[$n][1] < $optionalMeetingTimes[$i][1])
            $optionalMeetingTimes[$n][1] = $optionalMeetingTimes[$i][1];
        unset($optionalMeetingTimes[$i]);
    }
}

$optionalMeetingTimes = array_values($optionalMeetingTimes);
$meetingTimes = array();
foreach($optionalMeetingTimes as $optionalMeetingTime) {
    $meetingTimeStart = date( 'H:i',$optionalMeetingTime[0]);
    $meetingTimeEnd = date( 'H:i',$optionalMeetingTime[1]);
    $meetingTime = array($meetingTimeStart, $meetingTimeEnd);
    $meetingTimes[] = $meetingTime;
}



?>

<!DOCTYPE html>
<html>
<body>
<h1>Ben Hall's Theta Challenge</h1>
<table>
    <tr><th colspan="2">Available Time Blocks</th></tr>
    <tr>
        <th>Start Time</th>
        <th>End Time</th>
    </tr>
    <?php
    foreach ($meetingTimes as $meetingTime) {
        echo '<tr>';
        echo '<td>' . $meetingTime[0] . '</td>';
        echo '<td>' . $meetingTime[1] . '</td>';
        echo '</tr>';
    }
    ?>
</table>

</body>
</html>
