To use my solution edit the input variables at the top of the thetachallenge.php 
file as you choose, making sure to set the meeting duration in minutes.
Then push the thetachallenge.php file to a server that supports php and open the 
page in a web browser and the results will be displayed.